use SuperheroesDb;

ALTER TABLE Assistant ADD superheroId INTEGER;
ALTER TABLE Assistant ADD CONSTRAINT SuperheroAssistants FOREIGN KEY (superheroId) references Superhero(Id);