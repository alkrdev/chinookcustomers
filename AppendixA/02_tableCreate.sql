USE SuperheroesDb;

DROP TABLE IF EXISTS SuperheroesDb.Superhero;
CREATE TABLE Superhero (
	Id int PRIMARY KEY NOT NULL IDENTITY(1, 1),
	Name varchar(255) NOT NULL,
	Alias varchar(255) NOT NULL,
	Origin varchar(255) NOT NULL,
);

DROP TABLE IF EXISTS SuperheroesDb.Assistant;
CREATE TABLE Assistant (
	Id int PRIMARY KEY NOT NULL IDENTITY(1, 1),
	Name varchar(255) NOT NULL,
);

DROP TABLE IF EXISTS SuperheroesDb.Power;
CREATE TABLE Power (
	Id int PRIMARY KEY NOT NULL IDENTITY(1, 1),
	Name varchar(255) NOT NULL,
	Description varchar(255) NOT NULL,
);