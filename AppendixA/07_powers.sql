USE SuperheroesDb;

Declare @SuperheroId Integer
Declare @PowerId Integer


DELETE FROM Power;

INSERT INTO Power (Name, Description)
VALUES 
('Super Strength', 'Superhuman strength, capable of doing more damage than a normal human could, and lifting more than a normal human'),
('Flight', 'The ability to fly through the air'),
('Super Instinct', 'The ability to be extraordinarily aware of your surroundings and imminent dangers'),
('Hardened Skin', 'A reduction in damage taken when struck');

DELETE FROM SuperheroPowers;

SELECT @SuperheroId = Id FROM Superhero WHERE Name = 'Steve Rogers'
SELECT @PowerId = Id FROM Power WHERE Name = 'Super Strength'
INSERT INTO SuperheroPowers VALUES (@SuperheroId, @PowerId);

SELECT @PowerId = Id FROM Power WHERE Name = 'Super Instinct'
INSERT INTO SuperheroPowers VALUES (@SuperheroId, @PowerId);

SELECT @PowerId = Id FROM Power WHERE Name = 'Hardened Skin'
INSERT INTO SuperheroPowers VALUES (@SuperheroId, @PowerId);


SELECT @SuperheroId = Id FROM Superhero WHERE Name = 'Peter Parker'
SELECT @PowerId = Id FROM Power WHERE Name = 'Super Strength'
INSERT INTO SuperheroPowers VALUES (@SuperheroId, @PowerId);

SELECT @PowerId = Id FROM Power WHERE Name = 'Super Instinct'
INSERT INTO SuperheroPowers VALUES (@SuperheroId, @PowerId);

SELECT @PowerId = Id FROM Power WHERE Name = 'Hardened Skin'
INSERT INTO SuperheroPowers VALUES (@SuperheroId, @PowerId);


SELECT @SuperheroId = Id FROM Superhero WHERE Name = 'Steven Strange'
SELECT @PowerId = Id FROM Power WHERE Name = 'Flight'
INSERT INTO SuperheroPowers VALUES (@SuperheroId, @PowerId);

