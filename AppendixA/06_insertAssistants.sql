USE SuperheroesDb;

DELETE FROM Assistant;

INSERT INTO Assistant (Name, superheroId) VALUES
('Simon Cowell', (SELECT Id FROM Superhero WHERE Name = 'Steven Strange'));
INSERT INTO Assistant (Name, superheroId) VALUES
('Joan of Arc', (SELECT Id FROM Superhero WHERE Name = 'Peter Parker'));
INSERT INTO Assistant (Name, superheroId) VALUES
('Pegge Carter', (SELECT Id FROM Superhero WHERE Name = 'Steve Rogers'));
