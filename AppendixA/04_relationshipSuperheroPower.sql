USE SuperheroesDb;

CREATE TABLE SuperheroPowers (
	Id INTEGER PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	SuperheroId Integer NOT NULL,
	PowerId Integer NOT NULL
);

ALTER TABLE SuperheroPowers ADD CONSTRAINT Powers FOREIGN KEY (PowerId) references Power(Id);
ALTER TABLE SuperheroPowers ADD CONSTRAINT Superheroes FOREIGN KEY (SuperheroId) references Superhero(Id);