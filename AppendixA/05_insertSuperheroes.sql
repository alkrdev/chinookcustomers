USE SuperheroesDb;

DELETE FROM Superhero;

INSERT INTO Superhero (Name, Alias, Origin)
VALUES 
('Steve Rogers', 'Captain America', 'Injected with the super serum and frozen in Ice for 60 years'),
('Peter Parker', 'Spider-Man', 'Bitten by a radioactive spider'),
('Steven Strange', 'Doctor Strange', 'Hands were injured in car accident, so he sought out magic to help heal them')
