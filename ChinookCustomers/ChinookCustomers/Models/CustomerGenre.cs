﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookCustomers.Models
{
    public class CustomerGenre
    {
        public string Name { get; set; }
        public int GenreOccurrence { get; set; }
    }
}
