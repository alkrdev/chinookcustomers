﻿namespace ChinookCustomers.Models
{
    public class Customer
    {
        public Customer(int customerId, string firstName, string lastName, string country, string postalCode, string phone, string email)
        {
            CustomerId=customerId;
            FirstName=firstName;
            LastName=lastName;
            Country=country;
            PostalCode=postalCode;
            Phone=phone;
            Email=email;
        }

        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return $@"{CustomerId,-4}	{FirstName,-10}	{LastName,-15}	{Country,-15}	{PostalCode,-10}	{Phone,-20}	{Email,-15}";
        }
    }
}