﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookCustomers.Models
{
    public class CustomerSpender
    {
        public string FullName { get; set; }
        public double Total { get; set; }
    }
}
