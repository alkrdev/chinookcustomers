﻿using ChinookCustomers.DataAccess;
using ChinookCustomers.Models;


ICustomerRepository repo = new CustomerRepository();

Console.WriteLine("1. Read all the customers in the database");
var customers = repo.GetCustomers();
foreach (var customer in customers)
{
    Console.WriteLine(customer);
}

Console.WriteLine("\n\n2. Read a specific customer from the database (by id)");
var customerById = repo.GetCustomerByID(4);
Console.WriteLine(customerById);

Console.WriteLine("\n\n3. Read a specific customer by name");
var customerByName = repo.GetCustomerByName("Enrique");
Console.WriteLine(customerByName);

Console.WriteLine("\n\n4. Return a page of customers from the database. This should make use of the SQL limit and offset keywords.");
var selectionOfCustomers = repo.GetCustomersByLimitAndOffset(5, 10);
foreach (var customer in selectionOfCustomers)
{
    Console.WriteLine(customer);
}

Console.WriteLine("\n\n5. Add a new customer to the database");
Customer newCustomer = new Customer(-1, "Casper", "Kristensen", "Denmark", "8800", "31790942", "casperzk@gmail.com");
repo.InsertCustomer(newCustomer);
Console.WriteLine($"New Customer:");
Console.WriteLine($"{newCustomer}");

Console.WriteLine("\n\n6. Update an existing customer");
var customerToChange = repo.GetCustomerByName("Casper");

if (customerToChange != null)
{
    customerToChange.Country = "United Kingdom";
    customerToChange.LastName = "Sørensen";

    repo.UpdateCustomer(customerToChange);
    var changedCustomer = repo.GetCustomerByName("Casper");
    Console.WriteLine($"Newly updated Customer:");
    Console.WriteLine($"{changedCustomer}");
}

Console.WriteLine("\n\n7. Return the number of customers in each country, ordered descending");
var counts = repo.GetCountryCount();
foreach (var count in counts.OrderByDescending(x => x.Count))
{
    Console.WriteLine($"{count.Country}: {count.Count}");
}

Console.WriteLine("\n\n8. Customers who are the highest spenders, ordered descending");
var totals = repo.GetTotalSpentPerCustomer();
foreach (var total in totals.OrderByDescending(x => x.Total))
{
    Console.WriteLine($"{total.FullName}: {total.Total}");
}

Console.WriteLine("\n\n9. For a given customer, their most popular genre");
var givenId = 1;
CustomerGenre? cg = repo.GetPopularGenreByCustomerId(givenId);
Console.WriteLine($"User with ID: {givenId} results in:");
Console.WriteLine($"{cg.Name} with {cg.GenreOccurrence} occurrences");

Console.ReadKey();
