﻿using ChinookCustomers.Models;

namespace ChinookCustomers.DataAccess
{
    internal interface ICustomerRepository
    {
        IEnumerable<Customer> GetCustomers();
        IEnumerable<Customer> GetCustomersByLimitAndOffset(int limit, int offset);
        Customer? GetCustomerByID(int customerId);
        Customer? GetCustomerByName(string name);
        void InsertCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
        List<CustomerCountry> GetCountryCount();
        List<CustomerSpender> GetTotalSpentPerCustomer();
        CustomerGenre? GetPopularGenreByCustomerId(int Id);
    }
}
