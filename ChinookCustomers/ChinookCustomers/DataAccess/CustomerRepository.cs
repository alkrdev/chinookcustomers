using ChinookCustomers.Models;
using Microsoft.Data.SqlClient;
using System.Data;

namespace ChinookCustomers.DataAccess
{
    internal class CustomerRepository : ICustomerRepository
    {
        private readonly string connectionString = @"Data Source=DESKTOP-N2NV9C4\SQLEXPRESS;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public IEnumerable<Customer> GetCustomers()
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new("SELECT * FROM [Chinook].[dbo].[Customer]", connection);
            connection.Open();

            var customers = new List<Customer>();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Customer newCustomer = ParseCustomer(reader);
                    customers.Add(newCustomer);
                }
            }

            return customers;
        }
        public IEnumerable<Customer> GetCustomersByLimitAndOffset(int limit, int offset)
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new($"SELECT * FROM [Chinook].[dbo].[Customer] ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY", connection);
            command.Parameters.AddWithValue("@offset", offset);
            command.Parameters.AddWithValue("@limit", limit);

            connection.Open();

            var customers = new List<Customer>();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Customer newCustomer = ParseCustomer(reader);
                    customers.Add(newCustomer);
                }
            }

            return customers;
        }

        public Customer? GetCustomerByID(int customerId)
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new($"SELECT TOP 1 * FROM [Chinook].[dbo].[Customer] WHERE CustomerId = @customerId", connection);
            command.Parameters.AddWithValue("@customerId", customerId);

            connection.Open();
            using SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.Read())
            {
                return ParseCustomer(reader);
            }
            return null;
        }

        public Customer? GetCustomerByName(string name)
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new($"SELECT TOP 1 * FROM [Chinook].[dbo].[Customer] WHERE FirstName = @name", connection);
            command.Parameters.AddWithValue("@name", name);

            connection.Open();
            using SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.Read())
            {
                return ParseCustomer(reader);
            }
            return null;
        }

        public void InsertCustomer(Customer customer)
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new($@"INSERT INTO [Chinook].[dbo].[Customer] (FirstName, LastName, Country, PostalCode, Phone, Email) 
                                              VALUES(@param1, @param2, @param3, @param4, @param5, @param6); ", connection);

            command.Parameters.AddWithValue("@param1", customer.FirstName);
            command.Parameters.AddWithValue("@param2", customer.LastName);
            command.Parameters.AddWithValue("@param3", customer.Country);
            command.Parameters.AddWithValue("@param4", customer.PostalCode);
            command.Parameters.AddWithValue("@param5", customer.Phone);
            command.Parameters.AddWithValue("@param6", customer.Email);

            connection.Open();

            command.ExecuteNonQuery();
        }

        public void UpdateCustomer(Customer customer)
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new($@"UPDATE [Chinook].[dbo].[Customer] 
                                              SET 
                                                FirstName = @param1, 
                                                LastName = @param2, 
                                                Country = @param3, 
                                                PostalCode = @param4, 
                                                Phone = @param5, 
                                                Email = @param6 
                                              WHERE CustomerId = @Id", connection);

            command.Parameters.AddWithValue("@param1", customer.FirstName);
            command.Parameters.AddWithValue("@param2", customer.LastName);
            command.Parameters.AddWithValue("@param3", customer.Country);
            command.Parameters.AddWithValue("@param4", customer.PostalCode);
            command.Parameters.AddWithValue("@param5", customer.Phone);
            command.Parameters.AddWithValue("@param6", customer.Email);
            command.Parameters.AddWithValue("@Id", customer.CustomerId);

            connection.Open();

            command.ExecuteNonQuery();
        }

        public List<CustomerCountry> GetCountryCount ()
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new($"SELECT Country, COUNT(*) as Count FROM[Chinook].[dbo].[Customer] GROUP BY Country", connection);

            connection.Open();

            var counts = new List<CustomerCountry>();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    CustomerCountry cc = new()
                    {
                        Country = reader["Country"].ToString(),
                        Count = Convert.ToInt32(reader["Count"])
                    };

                    counts.Add(cc);
                }
            }

            return counts;
        }

        public List<CustomerSpender> GetTotalSpentPerCustomer()
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new(@"SELECT 
                                                (ISNULL(cus.FirstName, '') + ' ' + ISNULL(cus.LastName, '')) as FullName, 
                                                SUM(Total) as CustomerTotal 
                                             FROM [Chinook].[dbo].[Invoice] as inv 
                                             INNER JOIN[Chinook].[dbo].[Customer] as cus 
                                                ON(cus.CustomerId = inv.CustomerId) 
                                             GROUP BY(ISNULL(cus.FirstName, '') + ' ' + ISNULL(cus.LastName, ''))", connection);

            connection.Open();

            var customerSpenders = new List<CustomerSpender>();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    CustomerSpender cs = new()
                    {
                        FullName = reader["FullName"].ToString(),
                        Total = Convert.ToDouble(reader["CustomerTotal"])
                    };
                    customerSpenders.Add(cs);
                }
            }

            return customerSpenders;
        }
        public CustomerGenre? GetPopularGenreByCustomerId(int Id)
        {
            using SqlConnection connection = new(connectionString);
            using SqlCommand command = new(@"SELECT TOP 1 WITH TIES Genre.Name, COUNT(Genre.Name) as GenreOccurrence
                                            FROM [Chinook].[dbo].[Invoice]
                                            INNER JOIN [Chinook].[dbo].[InvoiceLine]
	                                            ON (Invoice.InvoiceId = InvoiceLine.InvoiceId)
                                            INNER JOIN [Chinook].[dbo].[Track]
	                                            ON (InvoiceLine.TrackId = Track.TrackId)
                                            INNER JOIN [Chinook].[dbo].[Genre]
	                                            ON (Track.GenreId = Genre.GenreId)
                                            WHERE Invoice.CustomerId = 1
                                            GROUP BY Genre.Name
                                            ORDER BY COUNT(Genre.Name) DESC", connection);

            //using SqlCommand command = new(@"with Base as (
            //                                    SELECT Invoice.CustomerId CustomerId, Genre.Name, COUNT(Genre.Name) as GenreOccurrence
            //                                        FROM Invoice
            //                                        INNER JOIN InvoiceLine 
            //                                            ON (Invoice.InvoiceId = InvoiceLine.InvoiceId)
            //                                        INNER JOIN Track
            //                                            ON (InvoiceLine.TrackId = Track.TrackId)
            //                                        INNER JOIN Genre
            //                                            ON (Track.GenreId = Genre.GenreId)
            //                                        GROUP BY Invoice.CustomerId, Genre.Name
            //                                ),
            //                                Layer1 as (
            //                                select *, ROW_NUMBER() OVER(PARTITION BY CustomerId order by GenreOccurrence desc) rowNumber
            //                                from Base
            //                                )

            //                                select * from Layer1
            //                                where rowNumber = 1", connection);



            command.Parameters.AddWithValue("@Id", Id);
            connection.Open();

            using SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.Read())
            {
                return new CustomerGenre
                {
                    Name = reader["Name"].ToString(),
                    GenreOccurrence = Convert.ToInt32(reader["GenreOccurrence"])
                };
            }
            return null;
        }

        private static Customer ParseCustomer(SqlDataReader reader)
        {
            return new Customer(
                Convert.ToInt32(reader["CustomerId"]),
                reader["FirstName"].ToString(),
                reader["LastName"].ToString(),
                reader["Country"].ToString(),
                reader["PostalCode"].ToString(),
                reader["Phone"].ToString(),
                reader["Email"].ToString()
            );
        }
    }
}
